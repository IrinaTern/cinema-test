package ru.irina.cinema.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.irina.cinema.exceptions.ReadableException;

//@ControllerAdvice(assignableTypes = {
//        ScheduleController.class,
//})
public class ErrorHandler {

    @ResponseBody
    @ExceptionHandler(ReadableException.class)
    public Response readableException(ReadableException exception) {
        return Response.error(exception.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public Response exception(Exception exception) {
        return Response.error("Что-то пошло не так");
    }
}
