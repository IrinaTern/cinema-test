package ru.irina.cinema.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.irina.cinema.dao.MovieCatalogDao;
import ru.irina.cinema.jdo.Movie;
import ru.irina.cinema.service.MovieService;

import java.util.List;

@Controller
public class MovieController {
    private final Logger logger = LoggerFactory.getLogger(MovieController.class);

    @Autowired
    private MovieCatalogDao movieCatalog;
    @Autowired
    private MovieService movieService;

    @ResponseBody
    @PostMapping(value = "/getAllMovie")
    public List<Movie> getAllMovie() {
        return movieCatalog.getAllMovies();
    }

    @RequestMapping(value = "/getMainPage")
    public String getMainPage() {
        return "index";
    }

    @RequestMapping(value = "/addNewMovie", method = RequestMethod.GET)
    public String addNewMovie() {
        return "addNewMovie";
    }


    @ResponseBody
    @RequestMapping(value = "/addNewMovie", method = RequestMethod.POST)
    public Response addNewMovie(@RequestParam String movieName,
                                @RequestParam Integer movieLength,
                                @RequestParam String movieDescription) {
        try {
            movieService.addMovie(movieName, movieLength, movieDescription);
            return Response.success();
        } catch (Exception e) {
            logger.error("Error:", e);
            return Response.error("Произошла ошибка при добавлении фильма.");
        }
    }

    @RequestMapping(value = "/deleteMovie", method = RequestMethod.POST)
    @ResponseBody
    public Response deleteMovie(@RequestParam Long movieId) {
        try {
            movieCatalog.deleteMovie(movieId);
            return Response.success();
        } catch (Exception e) {
            logger.error("Error: ", e);
            return Response.error("Произошла ошибка при удалении фильма.");
        }

    }

    @RequestMapping(value = "/getMovieIdByMovieName", method = RequestMethod.POST)
    @ResponseBody
    public Response getMovieIdByMovieName(@RequestParam String movieName) {
        try {
            movieCatalog.getMovieIdByMovieName(movieName);
            return Response.success();
        } catch (Exception e) {
            logger.error("Error:", e);
            return Response.error("Произошла ошибка при получении id фильма.");
        }
    }
}
