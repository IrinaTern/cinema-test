package ru.irina.cinema.controller;

public class Response {
    private enum Status {ERROR, OK}

    private Status status;
    private Object data;
    private String errorMessage;

    private Response() {
    }

    private Response(Status status) {
        this.status = status;
    }

    private Response(Status status, Object data) {
        this.status = status;
        this.data = data;
    }

    private Response(String errorMessage) {
        this.errorMessage = errorMessage;
        status = Status.ERROR;
    }

    public static Response success() {
        return new Response(Status.OK);
    }

    public static Response success(Object data) {
        return new Response(Status.OK, data);
    }

    public static Response error(String errorMessage) {
        return new Response(errorMessage);
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
