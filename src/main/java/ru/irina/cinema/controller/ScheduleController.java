package ru.irina.cinema.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.irina.cinema.dao.ScheduleDao;
import ru.irina.cinema.jdo.FilmSheduleInfo;
import ru.irina.cinema.service.ScheduleService;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
public class ScheduleController {
    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private ScheduleDao scheduleDao;

    private final Logger logger = LoggerFactory.getLogger(ScheduleController.class);


    @RequestMapping(value = "/getSchedule", method = RequestMethod.GET)
    public String getSchedule() {
        return "getSchedule";
    }

    @RequestMapping(value = "/getScheduleForDate", method = RequestMethod.GET)
    public String getScheduleToDate(Model model,
                                    @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        model.addAttribute(
                "scheduleList",
                scheduleService.getScheduleToDates(
                        Optional.ofNullable(date).orElse(new Date())));
        return "getScheduleForDate";

    }

    @PostMapping(value = "/getHallList")
    @ResponseBody
    public List<Long> getHallList() {
        return scheduleDao.getHallList();
    }

    @RequestMapping(value = "/getScheduleForDate", method = RequestMethod.POST)
    @ResponseBody
    public List<FilmSheduleInfo> getScheduleForDate(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) {
        return scheduleService.getScheduleToDates(date);
    }
}
