package ru.irina.cinema.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.irina.cinema.dao.SessionCatalogDao;

import java.util.Date;

@Controller
public class SessionController {

    private final Logger logger = LoggerFactory.getLogger(SessionController.class);
    @Autowired
    private SessionCatalogDao sessionCatalog;

    @RequestMapping(value = "/addSession", method = RequestMethod.POST)
    @ResponseBody
    public Response addSession(@RequestParam Long movieId,
                               @RequestParam Long hallId,
                               @RequestParam Long sessionDateTime,
                               @RequestParam Integer price
    ) {
        try {
            sessionCatalog.addSession(movieId, hallId, new Date(sessionDateTime), price);
            return Response.success();
        } catch (Exception e) {
            logger.error("Error:", e);
            return Response.error("Произошла ошибка при добавлении сеанса.");
        }
    }
    @RequestMapping(value = "/buyTicket/{sessionId}", method = RequestMethod.GET)
    public String getSessionHallBySessionId(@PathVariable Long sessionId) {
        return "buyTicket";
    }

}
