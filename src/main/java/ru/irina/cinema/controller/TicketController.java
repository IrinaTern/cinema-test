package ru.irina.cinema.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.irina.cinema.dao.TicketDao;
import ru.irina.cinema.service.HallService;
import ru.irina.cinema.service.TicketService;

@Controller
public class TicketController {

    @Autowired
    private TicketDao ticketDao;

    @Autowired
    private HallService hallService;

    @Autowired
    private TicketService ticketService;

    @RequestMapping(value = "/buyTicket")
    public String buyTicket() {
        return "buyTicket";
    }

    @RequestMapping(value = "/buyTicket-react/{sessionId}", method = RequestMethod.GET)
    public String buyTicketReact(@PathVariable Long sessionId) {
        return "buyTicket-react";
    }

    @RequestMapping(value = "/getTicketTitleBySessionId", method = RequestMethod.POST)
    @ResponseBody
    public Response getTicketTitleBySessionId(@RequestParam Long sessionId) {
        try {

            return Response.success(ticketDao.getTicketTitleBySessionId(sessionId));
        } catch (Exception e) {
            return Response.error("Произошла ошибка при получении данных для бронирования");
        }
    }

    @RequestMapping(value = "/getAllSeatsBySessionId", method = RequestMethod.POST)
    @ResponseBody
    public Response getAllSeatsBySessionId(@RequestParam Long sessionId) {
        try {

            return Response.success(ticketDao.getAllSeatsBySessionId(sessionId));
        } catch (Exception e) {
            return Response.error("Произошла ошибка при получении данных для бронирования");
        }
    }

    @RequestMapping(value = "/getHallLineList", method = RequestMethod.POST)
    @ResponseBody
    public Response getHallLineList(@RequestParam Long sessionId) {
        try {
            return Response.success(hallService.getHallLineList(sessionId));
        } catch (Exception e) {
            return Response.error("Произошла ошибка при получении схемы зала");
        }
    }

    @RequestMapping(value = "/checkReservation", method = RequestMethod.POST)
    @ResponseBody
    public Response checkReservation(@RequestParam Long sessionId, @RequestParam Long hallRecordId) {
        try {
            ticketService.checkReservation(sessionId, hallRecordId);
            return Response.success();
        } catch (Exception e) {
            return Response.error("Произошла ошибка бронирования");
        }
    }

}