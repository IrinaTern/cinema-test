package ru.irina.cinema.dao;

import ru.irina.cinema.jdo.HallRawData;

import java.util.List;

public interface HallDao {

    List<HallRawData> getAllSeatsBySessionId(Long sessionId);
    List<HallRawData> getSoldSeatsBySessionId(Long sessionId);


}
