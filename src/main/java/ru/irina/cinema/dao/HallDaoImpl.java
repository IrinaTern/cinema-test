package ru.irina.cinema.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.irina.cinema.jdo.HallRawData;
import ru.irina.cinema.utils.FileUtils;

import java.util.List;

@Repository
public class HallDaoImpl implements HallDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private final String GET_ALL_SEATS_BY_SESSION_ID = FileUtils.loadScriptFromFile("/sql/getAllSeatsBySessionId.sql");
    private final String GET_SOLD_SEATS_BY_SESSION_ID = FileUtils.loadScriptFromFile("/sql/getSoldSeatsBySessionId.sql");
    private final RowMapper<HallRawData> hallRowMapper = BeanPropertyRowMapper.newInstance(HallRawData.class);

    @Override
    public List<HallRawData> getAllSeatsBySessionId(Long sessionId){
        return jdbcTemplate.query(GET_ALL_SEATS_BY_SESSION_ID, hallRowMapper, sessionId);
    }

    @Override
    public List<HallRawData> getSoldSeatsBySessionId(Long sessionId){
        return jdbcTemplate.query(GET_SOLD_SEATS_BY_SESSION_ID, hallRowMapper, sessionId);

    }
   /* @PostConstruct
    public void init(){
        for (int h=1; h<=2; h++) {
            for (int i = 1; i <= 10; i++) {
                for (int j = 1; j <= 10; j++) {
                    jdbcTemplate.update("insert into halls (hall_id, line, seat) values (?, ?, ?)", h, i, j);
                }

            }
        }

    }*/

}
