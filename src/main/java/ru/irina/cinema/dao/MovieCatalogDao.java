package ru.irina.cinema.dao;

import ru.irina.cinema.jdo.Movie;

import java.util.List;

public interface MovieCatalogDao {

    List<Movie> getAllMovies();
    Movie getMovieById(Long id);
    void addMovie(String name, Integer length, String description);
    void deleteMovie(Long id);
    Long getMovieIdByMovieName(String name);

}
