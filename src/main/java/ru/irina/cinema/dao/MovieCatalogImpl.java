package ru.irina.cinema.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.irina.cinema.jdo.Movie;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class MovieCatalogImpl implements MovieCatalogDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    private final RowMapper<Movie> movieRowMapper = new RowMapper<Movie>() {

        @Override
        public Movie mapRow(ResultSet resultSet, int i) throws SQLException {

            Movie movie = new Movie();
            movie.setMovieId(resultSet.getLong("movie_Id"));
            movie.setMovieName(resultSet.getString("movie_Name"));
            movie.setMovieLength(resultSet.getInt("movie_Length"));
            movie.setMovieDescription(resultSet.getString("movie_Description"));

            return movie;
        }
    };

    @Override
    public List<Movie> getAllMovies() {
        return jdbcTemplate.query("select * from Movie_catalog", movieRowMapper);
    }

    @Override
    public Movie getMovieById(Long id) {
        String query = "select * from Movie_catalog where movie_id = ?";
        Movie movie = jdbcTemplate.queryForObject(query, movieRowMapper, id);
        return movie;
    }

    @Override
    public void addMovie(String name, Integer length, String description) {
        jdbcTemplate.update("insert into Movie_catalog(movie_name, movie_length, movie_description) values (?,?,?)", name, length, description);
    }

    @Override
    public void deleteMovie(Long id) {
        jdbcTemplate.update("delete from Movie_catalog where movie_id=?", id);
    }

    @Override
    public Long getMovieIdByMovieName(String name) {
        return jdbcTemplate.queryForObject("select movie_id from Movie_catalog where movie_name=?", Long.class, name);
    }
}
