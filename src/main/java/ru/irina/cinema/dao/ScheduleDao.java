package ru.irina.cinema.dao;

import ru.irina.cinema.jdo.Schedule;

import java.util.Date;
import java.util.List;

public interface ScheduleDao {

    List<Schedule> getScheduleList(Date startDay, Date endDay);
    List<Schedule> getScheduleList(Date date);
    List<Long> getHallList();



}
