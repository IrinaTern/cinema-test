package ru.irina.cinema.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.irina.cinema.jdo.Schedule;
import ru.irina.cinema.utils.FileUtils;

import java.util.Date;
import java.util.List;

@Repository
public class ScheduleDaoImpl implements ScheduleDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final String GET_SCHEDULE_BY_DATE = FileUtils.loadScriptFromFile("/sql/getScheduleList.sql");
    private final String GET_HALL_LIST = FileUtils.loadScriptFromFile("/sql/getHallList.sql");

    private final RowMapper<Schedule> scheduleRowMapper = BeanPropertyRowMapper.newInstance(Schedule.class);

    public List<Schedule> getScheduleList(Date startDay, Date endDay) {

        return jdbcTemplate.query(GET_SCHEDULE_BY_DATE, scheduleRowMapper, startDay, endDay);
    }

    @Override
    public List<Schedule> getScheduleList(Date date) {
        return jdbcTemplate.query(GET_SCHEDULE_BY_DATE, scheduleRowMapper, date);
    }

    @Override
    public List<Long> getHallList() {
        return jdbcTemplate.queryForList(GET_HALL_LIST, Long.class);
    }


}
