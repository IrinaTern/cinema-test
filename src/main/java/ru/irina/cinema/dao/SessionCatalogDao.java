package ru.irina.cinema.dao;

import ru.irina.cinema.jdo.Schedule;
import ru.irina.cinema.jdo.Session;

import java.util.Date;
import java.util.List;

public interface SessionCatalogDao {
    void addMovieToSchedule(Date sessionDateTime, Long movieId, Long hallId);
    void deleteMovieFromSchedule(Long id);
    List<Session> getAllSchedule();
    void addSession(Long movieId, Long hallId, Date sessionDateTime, Integer price);

}
