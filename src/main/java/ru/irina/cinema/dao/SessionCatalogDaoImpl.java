package ru.irina.cinema.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.irina.cinema.jdo.Schedule;
import ru.irina.cinema.jdo.Session;
import ru.irina.cinema.utils.FileUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;


@Repository
public class SessionCatalogDaoImpl implements SessionCatalogDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    private final String GET_SCHEDULE = FileUtils.loadScriptFromFile("/sql/schedule.sql");

    private final RowMapper<Schedule> scheduleRowMapper = BeanPropertyRowMapper.newInstance(Schedule.class);

    private final RowMapper<Session> sessionRowMapper = new RowMapper<Session>() {

        @Override
        public Session mapRow(ResultSet resultSet, int i) throws SQLException {

            Session session = new Session();
            session.setSessionId(resultSet.getLong("SESSION_ID"));
            session.setSessionDateTime(resultSet.getDate("SESSION_DATE_TIME"));
            session.setMovieId(resultSet.getLong("MOVIE_ID"));
            session.setHallId(resultSet.getLong("HALL_ID"));

            return session;
        }
    };

    @Override
    public void addMovieToSchedule(Date sessionDate, Long movieId, Long hallId) {
        jdbcTemplate.update("insert into SESSIONS (SESSION_ID, SESSION_DATE_TIME, MOVIE_ID, HALL_ID) values (sequence_session_id.nextval, ?, ?, ?)", sessionDate, movieId, hallId);
    }
    @Override
    public void deleteMovieFromSchedule(Long id) {
        jdbcTemplate.update("delete from SESSIONS where MOVIE_ID=?", id);
    }

    @Override
    public List<Session> getAllSchedule() {

        return jdbcTemplate.query("select * from SESSIONS", sessionRowMapper);
    }
    @Override
    public void addSession(Long movieId, Long hallId, Date sessionDateTime, Integer price) {
        jdbcTemplate.update("insert into SESSIONS ( SESSION_ID, MOVIE_ID, HALL_ID, SESSION_DATE_TIME, PRICE) values (sequence_session_id.nextval, ?, ?, ?, ?)", movieId, hallId, sessionDateTime, price);

        List<Long> hrid = jdbcTemplate.queryForList("select hall_record_id from halls where hall_id = ?", Long.class, hallId);

        for (Long id : hrid) {
            jdbcTemplate.update(
                    "insert into tickets( hall_record_id, session_id, status_id, price ) " +
                            "values (?, sequence_session_id.currVal, 0, ?)",
                    id, price
            );
        }

    }
}
