package ru.irina.cinema.dao;

import ru.irina.cinema.jdo.HallRawData;
import ru.irina.cinema.jdo.Ticket;
import ru.irina.cinema.jdo.TicketTitle;

import java.util.Date;
import java.util.List;

public interface TicketDao {

    TicketTitle getTicketTitleBySessionId(Long sessionId);
    List<HallRawData> getAllSeatsBySessionId(Long sessionId);
    void reserveSeat(Long sessionId, Long hallRecordId);
    void cancelReservation(Long sessionId, Long hallRecordId);
    Integer getSeatStatusId(Long sessionId, Long hallRecordId);
}
