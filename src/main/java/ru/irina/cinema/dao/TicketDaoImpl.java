package ru.irina.cinema.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.irina.cinema.jdo.HallRawData;
import ru.irina.cinema.jdo.TicketTitle;
import ru.irina.cinema.utils.FileUtils;

import java.util.List;

@Repository
public class TicketDaoImpl implements TicketDao {

    private final String GET_TICKET_TITLE_BY_SESSION_ID = FileUtils.loadScriptFromFile("/sql/getTicketTitleBySessionId.sql");
    private final String GET_ALL_SEATS_BY_SESSION_ID = FileUtils.loadScriptFromFile("/sql/getAllSeatsBySessionId.sql");

    private final RowMapper<TicketTitle> ticketTitleRowMapper = BeanPropertyRowMapper.newInstance(TicketTitle.class);
    private final RowMapper<HallRawData> hallRowMapper = BeanPropertyRowMapper.newInstance(HallRawData.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public TicketTitle getTicketTitleBySessionId(Long sessionId) {
        return jdbcTemplate.queryForObject(GET_TICKET_TITLE_BY_SESSION_ID, ticketTitleRowMapper, sessionId);
    }

    @Override
    public List<HallRawData> getAllSeatsBySessionId(Long sessionId) {
        return jdbcTemplate.query(GET_ALL_SEATS_BY_SESSION_ID, hallRowMapper, sessionId);
    }

    @Override
    public void reserveSeat(Long sessionId, Long hallRecordId) {
        jdbcTemplate.update("update TICKETS set STATUS_ID=? where HALL_RECORD_ID=? and SESSION_ID=?", 1, hallRecordId, sessionId);

    }

    @Override
    public void cancelReservation(Long sessionId, Long hallRecordId) {
        jdbcTemplate.update("update TICKETS set STATUS_ID=? where HALL_RECORD_ID=? and SESSION_ID=?", 0, hallRecordId, sessionId);

    }

    @Override
    public Integer getSeatStatusId(Long sessionId, Long hallRecordId) {
        return jdbcTemplate.queryForObject("select status_id from tickets where session_id=? and hall_record_id=?", Integer.class, sessionId, hallRecordId);
    }

}
