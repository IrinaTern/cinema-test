package ru.irina.cinema.exceptions;

public class ReadableException extends RuntimeException {
    public ReadableException() {
    }

    public ReadableException(String s) {
        super(s);
    }

    public ReadableException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ReadableException(Throwable throwable) {
        super(throwable);
    }

    public ReadableException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
