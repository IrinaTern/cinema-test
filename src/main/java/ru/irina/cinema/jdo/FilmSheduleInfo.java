package ru.irina.cinema.jdo;

import java.util.List;

public class FilmSheduleInfo {
    private Long movieId;
    private String movieName;
    private Integer movieLength;
    private List<HallScheduleInfo> hallScheduleInfo;

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public Integer getMovieLength() {
        return movieLength;
    }

    public void setMovieLength(Integer movieLength) {
        this.movieLength = movieLength;
    }

    public List<HallScheduleInfo> getHallScheduleInfo() {
        return hallScheduleInfo;
    }

    public void setHallScheduleInfo(List<HallScheduleInfo> hallScheduleInfo) {
        this.hallScheduleInfo = hallScheduleInfo;
    }
}
