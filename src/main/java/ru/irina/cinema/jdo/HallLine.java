package ru.irina.cinema.jdo;

import java.util.List;
import java.util.Objects;

public class HallLine {
    Integer line;
    List<SeatInfo> seatInfoList;

    public HallLine(Integer line, List<SeatInfo> seatInfoList) {
        this.line = line;
        this.seatInfoList = seatInfoList;
    }

    public HallLine() {
    }

    public Integer getLine() {
        return line;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    public List<SeatInfo> getSeatInfoList() {
        return seatInfoList;
    }

    public void setSeatInfoList(List<SeatInfo> seatInfoList) {
        this.seatInfoList = seatInfoList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HallLine hallLine = (HallLine) o;
        return line.equals(hallLine.line) &&
                seatInfoList.equals(hallLine.seatInfoList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(line, seatInfoList);
    }
}
