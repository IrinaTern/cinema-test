package ru.irina.cinema.jdo;

public class HallRawData {
    private Long hallRecordId;
    private Long hallId;
    private Integer line;
    private Integer seat;
    private Integer statusId;

    public HallRawData(Long hallRecordId, Long hallId, Integer line, Integer seat, Integer statusId) {
        this.hallRecordId = hallRecordId;
        this.hallId = hallId;
        this.line = line;
        this.seat = seat;
        this.statusId = statusId;
    }

    public HallRawData() {
    }

    public Integer getLine() {
        return line;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }

    public Long getHallId() {
        return hallId;
    }

    public void setHallId(Long hallId) {
        this.hallId = hallId;
    }

    public Long getHallRecordId() {
        return hallRecordId;
    }

    public void setHallRecordId(Long hallRecordId) {
        this.hallRecordId = hallRecordId;
    }

    public boolean isValid() {
        return hallRecordId != null && hallId != null && line != null && seat != null;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }
}
