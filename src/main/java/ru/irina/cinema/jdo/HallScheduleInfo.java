package ru.irina.cinema.jdo;

import java.util.Date;
import java.util.List;

public class HallScheduleInfo {
    private Long hallId;
    private List<SessionIdTimeInfo> sessionIdTimeInfoList;

    public Long getHallId() {
        return hallId;
    }

    public void setHallId(Long hallId) {
        this.hallId = hallId;
    }

    public List<SessionIdTimeInfo> getSessionIdTimeInfoList() {
        return sessionIdTimeInfoList;
    }

    public void setSessionIdTimeInfoList(List<SessionIdTimeInfo> sessionIdTimeInfoList) {
        this.sessionIdTimeInfoList = sessionIdTimeInfoList;
    }
}
