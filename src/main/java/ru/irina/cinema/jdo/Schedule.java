package ru.irina.cinema.jdo;

import java.util.Date;

public class Schedule {
    private Long movieId;
    private String movieName;
    private Integer movieLength;
    private Long hallId;
    private Date sessionDateTime;
    private Long sessionId;

    public Schedule() {
    }

    public Schedule(Long movieId, String movieName, Integer movieLength, Long hallId, Date sessionDateTime, Long sessionId) {
        this.movieId = movieId;
        this.movieName = movieName;
        this.movieLength = movieLength;
        this.hallId = hallId;
        this.sessionDateTime = sessionDateTime;
        this.sessionId = sessionId;
    }

    public Long getHallId() {
        return hallId;
    }

    public void setHallId(Long hallId) {
        this.hallId = hallId;
    }

    public Date getSessionDateTime() {
        return sessionDateTime;
    }

    public void setSessionDateTime(Date sessionDateTime) {
        this.sessionDateTime = sessionDateTime;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public Integer getMovieLength() {
        return movieLength;
    }

    public void setMovieLength(Integer movieLength) {
        this.movieLength = movieLength;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }
}
