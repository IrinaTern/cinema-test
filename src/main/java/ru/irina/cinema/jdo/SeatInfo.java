package ru.irina.cinema.jdo;

import java.util.Objects;

public class SeatInfo {
    private Long seatId;
    private Integer seat;
    private Integer statusId;

    public SeatInfo(Long seatId, Integer seat, Integer statusId) {
        this.seatId = seatId;
        this.seat = seat;
        this.statusId = statusId;

    }

    public SeatInfo() {
    }

    public Long getSeatId() {
        return seatId;
    }

    public void setSeatId(Long seatId) {
        this.seatId = seatId;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SeatInfo seatInfo = (SeatInfo) o;
        return Objects.equals(seatId, seatInfo.seatId) &&
                Objects.equals(seat, seatInfo.seat) &&
                Objects.equals(statusId, seatInfo.statusId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(seatId, seat, statusId);
    }
}
