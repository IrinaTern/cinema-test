package ru.irina.cinema.jdo;

import java.util.Date;

public class SessionIdTimeInfo {
    private Long sessionId;
    private Date startTime;

    public SessionIdTimeInfo() {
    }

    public SessionIdTimeInfo(Long sessionId, Date startTime) {
        this.sessionId = sessionId;
        this.startTime = startTime;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
}
