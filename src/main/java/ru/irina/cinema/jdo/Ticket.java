package ru.irina.cinema.jdo;

public class Ticket {
    private Long ticketId;
    private Long sessionId;
    private Long statusId;
    private Long hallRecordId;
    private Integer price;

    public Long getTicketId() {
        return ticketId;
    }

    public void setTicketId(Long ticketId) {
        this.ticketId = ticketId;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Long getHallRecordId() {
        return hallRecordId;
    }

    public void setHallRecordId(Long hallRecordId) {
        this.hallRecordId = hallRecordId;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
