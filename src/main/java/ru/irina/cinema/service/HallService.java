package ru.irina.cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.irina.cinema.dao.HallDao;
import ru.irina.cinema.exceptions.InvalidHallDataException;
import ru.irina.cinema.jdo.HallRawData;
import ru.irina.cinema.jdo.HallLine;
import ru.irina.cinema.jdo.SeatInfo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class HallService {

    @Autowired
    private HallDao hallDao;

    static void checkRawData(List<HallRawData> rawData) {
        if (rawData == null || rawData.isEmpty()) {
            throw new InvalidHallDataException();
        }
        for (HallRawData rawDatum : rawData) {
            if (!rawDatum.isValid()) {
                throw new InvalidHallDataException();
            }

        }
    }

    static HallLine getHallLine(List<HallRawData> rawData, Integer line) {

        HallLine hallLine = new HallLine();
        List<SeatInfo> seatInfoList = new ArrayList<>();
        hallLine.setLine(line);
        hallLine.setSeatInfoList(seatInfoList);

        for (HallRawData rawDatum : rawData) {
            if (rawDatum.getLine().equals(line)) {
                SeatInfo seatInfo = new SeatInfo();
                seatInfo.setSeatId(rawDatum.getHallRecordId());
                seatInfo.setSeat(rawDatum.getSeat());
                seatInfo.setStatusId(rawDatum.getStatusId());
                seatInfoList.add(seatInfo);
            }
        }
        return hallLine;
    }

    public List<HallLine> getHallLineList(Long sessionId) {

        List<HallLine> hallLineList = new ArrayList<>();
        Set<Integer> lineSet = new HashSet<>();
        List<HallRawData> rawData = hallDao.getAllSeatsBySessionId(sessionId);
        checkRawData(rawData);

        for (HallRawData rawDatum : rawData) {
            lineSet.add(rawDatum.getLine());
        }

        for (Integer line : lineSet) {
            hallLineList.add(getHallLine(rawData, line));
        }
        return hallLineList;

    }


}
