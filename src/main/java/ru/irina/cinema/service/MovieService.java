package ru.irina.cinema.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.irina.cinema.dao.MovieCatalogDao;

@Service
public class MovieService {

    @Autowired
    private MovieCatalogDao movieCatalogDao;

    public void addMovie(String name, Integer length, String description){
        checkMovieInfo(name, length, description);
    }

    private void checkMovieInfo(String name, Integer length, String description){
        if (length<0){
            throw new RuntimeException("Продолжительность фильма < 0!");
        }
        movieCatalogDao.addMovie(name, length, description);
    }

}
