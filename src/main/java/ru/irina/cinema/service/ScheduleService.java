package ru.irina.cinema.service;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.irina.cinema.dao.ScheduleDao;
import ru.irina.cinema.jdo.FilmSheduleInfo;
import ru.irina.cinema.jdo.HallScheduleInfo;
import ru.irina.cinema.jdo.Schedule;
import ru.irina.cinema.jdo.SessionIdTimeInfo;

import java.util.*;

@Service
public class ScheduleService {

    @Autowired
    private ScheduleDao scheduleDao;

    private final Logger logger = LoggerFactory.getLogger(ScheduleDao.class);

    public List<FilmSheduleInfo> getScheduleToDates(Date dateFrom, Date dateTo) {
        return buildFilmScheduleInfo(scheduleDao.getScheduleList(getStartDay(dateFrom), getEndDay(dateTo)));
    }

    public List<FilmSheduleInfo> getScheduleToDates(Date date) {
        return buildFilmScheduleInfo(scheduleDao.getScheduleList(getStartDay(date), getEndDay(date)));
    }

    private Date getStartDay(Date date) {
        return new DateTime(date).withTimeAtStartOfDay().toDate();
    }

    private Date getEndDay(Date date) {
        return new DateTime(date).plusDays(1).withTimeAtStartOfDay().toDate();
    }

    private List<FilmSheduleInfo> buildFilmScheduleInfo(List<Schedule> rawData) {
        Set<Long> setMovieId = new HashSet<>();
        List<FilmSheduleInfo> filmScheduleInfo = new ArrayList<>();

        for (Schedule rawDatum : rawData) {
            if (setMovieId.contains(rawDatum.getMovieId())) {
                continue;
            }

            setMovieId.add(rawDatum.getMovieId());

            FilmSheduleInfo scheduleInfo = new FilmSheduleInfo();
            scheduleInfo.setMovieId(rawDatum.getMovieId());
            scheduleInfo.setMovieName(rawDatum.getMovieName());
            scheduleInfo.setMovieLength(rawDatum.getMovieLength());
            scheduleInfo.setHallScheduleInfo(getHallScheduleInfo(scheduleInfo.getMovieId(), rawData));

            filmScheduleInfo.add(scheduleInfo);

        }

        return filmScheduleInfo;
    }

    private List<HallScheduleInfo> getHallScheduleInfo(Long movieId, List<Schedule> rawData) {
        Set<Long> setHallId = new HashSet<>();
        List<HallScheduleInfo> hallScheduleInfo = new ArrayList<>();

        for (Schedule rawDatum : rawData) {
            if (rawDatum.getMovieId().equals(movieId)) {
                if (setHallId.contains(rawDatum.getHallId())) {
                    continue;
                }

                setHallId.add(rawDatum.getHallId());

                HallScheduleInfo hallSchedule = new HallScheduleInfo();
                hallSchedule.setHallId(rawDatum.getHallId());
                hallSchedule.setSessionIdTimeInfoList(getSessionIdDateList(movieId, rawDatum.getHallId(), rawData));

                hallScheduleInfo.add(hallSchedule);
            }
        }
        return hallScheduleInfo;
    }

    private List<SessionIdTimeInfo> getSessionIdDateList(Long movieId, Long hallId, List<Schedule> rawData) {
        List<SessionIdTimeInfo> sessionIdDateList = new ArrayList<>();
        LocalDateTime time = new LocalDateTime().minusYears(10);
        for (Schedule rawDatum : rawData) {
            if (rawDatum.getMovieId().equals(movieId)) {
                if (rawDatum.getHallId().equals(hallId)) {
                    if (time.isBefore(new LocalDateTime(rawDatum.getSessionDateTime()))) {
                        sessionIdDateList.add(new SessionIdTimeInfo(rawDatum.getSessionId(), rawDatum.getSessionDateTime()));
                        time = new LocalDateTime(rawDatum.getSessionDateTime()).plusMinutes(rawDatum.getMovieLength()).plusMinutes(20);
                    } else {
                        logger.error("Произошла ошибка при формировании листа дат: hall:{}, movieId:{}, date:{}, time:{}, dateNextFilm:{} ", hallId, movieId, sessionIdDateList.get(sessionIdDateList.size() - 1), time, rawDatum.getSessionDateTime());
                        throw new RuntimeException("Произошла ошибка при формировании листа дат");
                    }
                }
            }
        }
        return sessionIdDateList;
    }
}

