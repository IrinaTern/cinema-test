package ru.irina.cinema.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.irina.cinema.dao.HallDao;
import ru.irina.cinema.dao.TicketDao;
import ru.irina.cinema.exceptions.InvalidHallDataException;
import ru.irina.cinema.jdo.HallLine;
import ru.irina.cinema.jdo.HallRawData;
import ru.irina.cinema.jdo.SeatInfo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class TicketService {

    @Autowired
    private TicketDao ticketDao;

    public void checkReservation(Long sessionId, Long hallRecordId) {
        Integer statusId = ticketDao.getSeatStatusId(sessionId, hallRecordId);
        if (statusId == 0) {
             ticketDao.reserveSeat(sessionId, hallRecordId);
        } else if (statusId == 1) {
             ticketDao.cancelReservation(sessionId, hallRecordId);
        } else if (statusId == 2) {
            throw new RuntimeException("Место недоступно для бронирования");
        } else {
            throw new RuntimeException("Ошибка. Бронирование этого места невозможно");
        }
    }

}
