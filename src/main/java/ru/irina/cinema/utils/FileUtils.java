package ru.irina.cinema.utils;

import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.file.Files;

public class FileUtils {
    public static String loadScriptFromFile(String filePath) {

        try {
            return new String(Files.readAllBytes(new ClassPathResource(filePath).getFile().toPath()));
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }

    }
}
