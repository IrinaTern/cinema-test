
select
  hh.hall_id             as hallId,
  hh.line                as line,
  hh.seat                as seat,
  hh.hall_record_id      as hallRecordId,
  tt.status_id           as statusId
from sessions ss
       join halls hh on ss.hall_id = hh.hall_id
       join tickets tt on (hh.hall_record_id = tt.hall_record_id and ss.session_id = tt.session_id )
where ss.session_id = ?
order by hh.hall_id, hh.line, hh.seat;