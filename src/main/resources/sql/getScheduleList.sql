select
  movie_catalog.movie_id as movieId,
  movie_name             as movieName,
  movie_length           as movieLength,
  hall_id                as hallId,
  SESSION_DATE_TIME      as sessionDateTime,
  session_id             as sessionId
from movie_catalog
       join sessions on movie_catalog.movie_id = sessions.movie_id
where
  SESSION_DATE_TIME >= ?
  AND SESSION_DATE_TIME <= ?
order by movieName, hall_id, SESSION_DATE_TIME


