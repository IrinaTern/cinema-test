select
  halls.hall_id         as hallId,
  seat                  as seat,
  line                  as line

from halls join sessions on halls.hall_id = sessions.hall_id join tickets on halls.hall_record_id = tickets.hall_record_id
where
  sessions.session_id = ?
  and status_id = 2
order by halls.hall_id, line, seat;


