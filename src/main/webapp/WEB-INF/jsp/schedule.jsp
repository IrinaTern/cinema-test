<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>


<html>
<head>
    <title> Schedule </title>
</head>
<body>
Hello!

<table border="1" width="100%" cellpadding="5">
    <c:forEach items="${schedule}" var="schedule">
        <tr>
            <th>${schedule.sessionDateTime}</th>
            <th>${schedule.movieName}</th>
            <th>${schedule.movieLength}</th>
            <th>${schedule.movieDescription}</th>
        </tr>
    </c:forEach>
</table>
</body>
</html>