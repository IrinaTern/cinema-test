function RenderHall() {
    const {useState, useEffect} = React;

    const [hallLines, setHallLines] = useState([]);

    const [titleData, setTitleData] = useState({
        dateAndTime: null,
        movieName: null,
        hallId: null,
    });

    useEffect(() => {
        loadHallData();
        loadTitleData();
    }, []);

    function getSessionId() {
        const URL = document.location.href;
        const arr = URL.split('/');
        return arr[arr.length - 1];
    }

    function loadHallData() {
        const data = {sessionId: getSessionId()};
        $.post("/cinema/getHallLineList", data).done((response) => setHallLines(response.data));
    }

    function loadTitleData() {
        const data = {sessionId: getSessionId()};
        $.post("/cinema/getTicketTitleBySessionId", data).done(
            (response) => setTitleData({
                dateAndTime: response.data.sessionDateTime,
                movieName: response.data.movieName,
                hallId: response.data.hallId,
            })
        );
    }

    function seatButtonClickHandler(seatId) {

        let data = {
            sessionId: getSessionId(),
            hallRecordId: seatId
        };

        const responseHandler = (response) => {
            if (response.status === "OK") {
                loadHallData();
                return;
            }
            alert("Error:" + response.errorMessage);
        };

        $.post("/cinema/checkReservation", data).done(responseHandler);

    }

    return (
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-4">
                    {titleData && <TitleInfo {...titleData}/>}
                </div>
                <div className="col-md-4">
                    <table className="table table-hover">
                        <tbody>
                        {
                            hallLines && hallLines.map(
                                (line) => <HallLine key={line.line} hallLine={line}
                                                    clickHandler={seatButtonClickHandler}/>
                            ) || <tr style={{display: "none"}}/>
                        }
                        </tbody>
                    </table>
                </div>
                <div className="col-md-8">
                </div>
            </div>
        </div>
    );

}

function HallLine({hallLine, clickHandler}) {
    return (
        hallLine &&
        <tr>
            <td>{hallLine.line}</td>
            {
                hallLine.seatInfoList &&
                hallLine.seatInfoList.map(
                    (info) => <SeatInfo key={info.seatId} seatInfo={info} clickHandler={clickHandler}/>
                )
            }
        </tr>
        || <tr style="display: none"/>
    );
}


function SeatInfo({seatInfo, clickHandler}) {
    const colors = {
        "0": "white",
        "1": "gray"
    };

    const color = colors["" + seatInfo.statusId] || 'white';

    return (
        <td>
            <button style={{backgroundColor: color}} onClick={() => clickHandler(seatInfo.seatId)}>
                {seatInfo.seat}
            </button>
        </td>
    )
}

function TitleInfo({dateAndTime, movieName, hallId}) {

    return (
        dateAndTime && movieName && hallId &&
        <table>
            <tbody>
            <tr>
                <td>Date and time:</td>
                <td>{moment(dateAndTime).format('L') + ' ' + moment(dateAndTime).format('LT')}</td>
            </tr>
            <tr>
                <td>Movie:</td>
                <td>{movieName}</td>
            </tr>
            <tr>
                <td>Hall:</td>
                <td>{hallId}</td>
            </tr>

            </tbody>
        </table>
        || <div style={{display: "none"}}/>
    )
}


ReactDOM.render(
    <RenderHall/>,
    document.getElementById('root')
);


