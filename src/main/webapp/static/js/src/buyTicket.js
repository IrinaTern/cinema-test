;(function () {
    var URL = document.location.href;
    var arr = URL.split('/');
    var sessionId = arr[arr.length - 1];
    var data = {sessionId: sessionId};

    function init() {
        $.post("/cinema/getTicketTitleBySessionId", data).done(responseHandler);
        $.post("/cinema/getHallLineList", data).done(hallViewBuilder);
    }

    init();

    function responseHandler(response) {
        ticketTitleInfoBuilder(response);
    }

    function ticketTitleInfoBuilder(response) {
        $('#ticket-date').html(moment(response.data.sessionDateTime).format('L') + ' ' + moment(response.data.sessionDateTime).format('LT'));
        $('#ticket-movie-name').html(response.data.movieName);
        $('#ticket-hall-id').html(response.data.hallId);
    }

    function hallViewBuilder(response) {
        var arrayList = [];
        arrayList.push('<tr>' + '<td colspan="11" align="center">' + 'ЭКРАН' + '</td>' + '</tr>');
        response.data.forEach(function (hallLine) {
            arrayList.push(buildTableRow(hallLine));
        });
        $('#hallView').html(arrayList.join(""));
        $('#hallView button').on("click", reserveSeat);

    }

    function buildTableRow(hallLine) {
        return '<tr>'
            + '<td>' + hallLine.line + '</td>'
            + buildSeatInfoRow(hallLine.seatInfoList)
            + '</tr>';
    }

    function buildSeatInfoRow(seatInfoList) {

        var arrayList = [];
        seatInfoList.forEach(function (seatInfo) {
            arrayList.push('<td>' + colorButton(seatInfo) + '</td>')
        });
        return arrayList.join("");
    }

    function reserveSeat(e) {
        e.stopPropagation();
        var btn = e.target;
        var seatId = $(btn).attr("data-seat-id");
        var data = {
            sessionId: sessionId,
            hallRecordId: seatId
        };

        $.post("/cinema/checkReservation", data).done(responseHandler);

        function responseHandler(response) {
            if (response.status === "OK") {
                init();
                return;
            }
            alert("Error:" + response.errorMessage);
        }

    }

    function colorButton(seatInfo) {
        if (seatInfo.statusId === 1) {
            return '<button id="'+ seatInfo.seatId +'" style="background-color:gray" data-seat-id="'
                + seatInfo.seatId + '">'
                + seatInfo.seat
                + '</button>';
        } else if (seatInfo.statusId === 0) {
            return '<button id="' + seatInfo.seatId + '" style="background-color:white" data-seat-id="'
                + seatInfo.seatId + '">'
                + seatInfo.seat
                + '</button>';
        }
    }

})();
