;(function () {

    var ADD_MOVIE_BTN_ID = "add-film-btn";
    var MOVIE_NAME_SELECTOR = '[name="movieName"]';
    var MOVIE_LENGTH_SELECTOR = '[name="movieLength"]';
    var MOVIE_DESCRIPTION_SELECTOR = '[name="movieDescription"]';
    var MOVIE_ID_SELECTOR = '[name="movieId"]';

    function init () {
        $("#" + ADD_MOVIE_BTN_ID).on("click", btnClickHandler);
        refreshFilms();
    }

    init();

    function btnClickHandler(e) {
        e.stopPropagation();
        var name = $(MOVIE_NAME_SELECTOR).val();
        var length = $(MOVIE_LENGTH_SELECTOR).val();
        var desc = $(MOVIE_DESCRIPTION_SELECTOR).val();
        var id = $(MOVIE_ID_SELECTOR).val();


        var data = {
            movieName: name,
            movieLength: length,
            movieDescription: desc,
            movieId: id
        };

        $.post("/cinema/addNewMovie", data).done(responseHandler);

        function responseHandler(response) {
            if (response.status === "OK") {
                alert("Film added");
                refreshFilms();
                return;
            }

            alert("Error:" + status.errorMessage);
        }
    }

    function deleteBtnClickHandler(e) {
        e.stopPropagation();
        var btn =e.target;
        var id = $(btn).attr("data-film-id");
        var data ={movieId: id};
        $.post("/cinema/deleteMovie", data).done(responseHandler);

        function responseHandler(response) {
            if (response.status === "OK") {
                alert("Film deleted");
                refreshFilms();
                return;
            }
            alert("Error:" + response.errorMessage);
        }
    }



    function refreshFilms() {

        $.post("/cinema/getAllMovie").done(responseHandler);


        function responseHandler(data) {
            var arrayList = [];
            data.forEach(function (movie) {
                arrayList.push( buildTableRow(movie));
            });
            $('#films').html( arrayList.join(""));
            $('#films button').on("click", deleteBtnClickHandler);


        }
    }
    function buildTableRow(movie){
        return '<tr>' + '<td>' + movie.movieName + '</td>'
                      + '<td>' + movie.movieLength + '</td>'
                      + '<td>' + movie.movieDescription + '</td>'
                      + '<td><button data-film-id="'+ movie.movieId +'">'+ 'delete' + '</button></td>'+
               '</tr>';
    }

})();
