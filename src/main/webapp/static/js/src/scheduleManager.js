;(function () {

    var ADD_SESSION_BTN_ID = "add-session-btn";
    var MOVIE_NAME_SELECTOR = '[name="movieName"]';
    var HALL_SELECTOR = '[name="hallId"]';
    var SESSION_DATE_SELECTOR = '[name="sessionDate"]';
    var SESSION_TIME_SELECTOR = '[name="sessionTime"]';
    var PRICE_SELECTOR = '[name="price"]';

    function init() {
        initMovieSelect();
        initHallSelect();
        $("#" + ADD_SESSION_BTN_ID).on("click", buttonClickHandler);
    }

    init();

    function initMovieSelect() {

        $.post("/cinema/getAllMovie").done(function (resp) {
            var movieOptions = [];
            movieOptions.push('<option label="Select movie">Select movie</option>');
            resp.forEach(function (movie) {
                movieOptions.push(buildOptions(movie))
            });

            function buildOptions(movie) {
                return '<option value="' + movie.movieId + '">' + movie.movieName + '</option>';
            }
            $(MOVIE_NAME_SELECTOR).html(movieOptions.join(""));
        });

    }

    function initHallSelect() {
        $.post("/cinema/getHallList").done(function (hallIds) {
            var hallIdOptions = [];
            hallIdOptions.push('<option label="Select hall">Select hall</option>');
            hallIds.forEach(function (hallId) {

                hallIdOptions.push(buildHallIdOptions(hallId))
            });

            function buildHallIdOptions(hallId) {
                return '<option value="' + hallId + '">' + hallId + '</option>';
            }
            $(HALL_SELECTOR).html(hallIdOptions.join(""));
        })
    }

    function buttonClickHandler(e) {
        e.stopPropagation();

        var movieId = $(MOVIE_NAME_SELECTOR).val();
        var hallId = $(HALL_SELECTOR).val();
        var sessionDateTime = new Date($(SESSION_DATE_SELECTOR).val()+' '+$(SESSION_TIME_SELECTOR).val());
        var price = $(PRICE_SELECTOR).val();
        var data = {
            movieId: movieId,
            hallId: hallId,
            sessionDateTime: sessionDateTime,
            price: price
        };
        $.post("/cinema/addSession", data).done(responseHandler);

        function responseHandler(response) {
            if (response.status==="OK") {
                alert("Session added");
                refreshSchedule();
                return;
            }
            alert("Error:" + response.errorMessage);

        }
    }

      function refreshSchedule() {
        var date = new Date($(SESSION_DATE_SELECTOR).val());
        var data = {
            date: date
        };

          $.post("/cinema/getScheduleForDate", data).done(dataHandler);

          function dataHandler(listFilmScheduleInfo) {
              var arrayList = [];
              listFilmScheduleInfo.forEach(function (filmScheduleInfo) {
                  arrayList.push(buildTableRow(filmScheduleInfo));
              });
              $('#schedule').html(arrayList.join(""));
          }
   }

   function buildTableRow(filmScheduleInfo) {
       return scheduleBuilder(filmScheduleInfo);

   }

   function timeListBuilder(sessionIdTimeInfoList) {
       var arrayList = [];
       sessionIdTimeInfoList.forEach(function (sessionIdTimeInfo) {
           arrayList.push('<td>' + moment(sessionIdTimeInfo.startTime).format("HH:mm") + '</td>');
       });
       return arrayList.join("");
   }
   function hallScheduleBuilder(hallScheduleList) {
        var arrayList = [];
        hallScheduleList.forEach(function (hallSchedule) {
            arrayList.push(
                '<tr>'
                + '<td>' + 'Hall ' + hallSchedule.hallId + '</td>'
                + '<td>' + timeListBuilder(hallSchedule.sessionIdTimeInfoList) + '</td>'
                + '</tr>'
            );
        });
        return arrayList.join("");

   }
   function scheduleBuilder(filmScheduleInfo) {

        return '<tr>'
            + '<td>' + filmScheduleInfo.movieName + '</td>'
            +'</tr>'
            + hallScheduleBuilder(filmScheduleInfo.hallScheduleInfo);

   }

})();
