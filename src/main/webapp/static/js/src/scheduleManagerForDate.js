;(function () {

    var SHOW_SCHEDULE_BTN_ID = "show-schedule-btn";
    var SESSION_DATE_SELECTOR = '[name="sessionDate"]';

    function init() {
        $("#" + SHOW_SCHEDULE_BTN_ID).on("click", getScheduleForDate);

    }

    init();



      function getScheduleForDate() {
        var date = new Date($(SESSION_DATE_SELECTOR).val());
        var data = {
            date: date
        };

          $.post("/cinema/getScheduleForDate", data).done(dataHandler);

          function dataHandler(listFilmScheduleInfo) {
              var arrayList = [];
              listFilmScheduleInfo.forEach(function (filmScheduleInfo) {
                  arrayList.push(buildTableRow(filmScheduleInfo));
              });
              $('#schedule').html(arrayList.join(""));


          }
   }

   function buildTableRow(filmScheduleInfo) {
       return scheduleBuilder(filmScheduleInfo);

   }

   function timeListBuilder(sessionIdTimeInfoList) {
       var arrayList = [];
       sessionIdTimeInfoList.forEach(function (sessionIdTimeInfo) {
           arrayList.push('<td>' + '<a href="/cinema/buyTicket/' + sessionIdTimeInfo.sessionId + '">' + moment(sessionIdTimeInfo.startTime).format("HH:mm")+ '</a>' + '</td>');
       });
       return arrayList.join("");

   }
   function hallScheduleBuilder(hallScheduleList) {
        var arrayList = [];
        hallScheduleList.forEach(function (hallSchedule) {
            arrayList.push(
                '<tr>'
                + '<td>' + 'Hall ' + hallSchedule.hallId + '</td>'
                + '<td>' + timeListBuilder(hallSchedule.sessionIdTimeInfoList) + '</td>'
                + '</tr>'
            );
        });
        return arrayList.join("");

   }
   function scheduleBuilder(filmScheduleInfo) {

        return '<tr>'
            + '<td>' + filmScheduleInfo.movieName + '</td>'
            +'</tr>'
            + hallScheduleBuilder(filmScheduleInfo.hallScheduleInfo);


   }


})();
