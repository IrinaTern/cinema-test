import * as React from "react";
import {IHallLine} from "../interfaces/interfaces";
import {SeatInfo} from "./SeatInfo";

export const HallLine: React.FC<IHallLine> = (hallLine) =>
    hallLine &&
    <tr>
        <td>{hallLine.line}</td>
        {
            hallLine.seatInfoList &&
            hallLine.seatInfoList.map((info) =>
                <SeatInfo
                    key={info.seatId}
                    seatId={info.seatId}
                    seat={info.seat}
                    statusId={info.statusId}
                    clickHandler={hallLine.clickHandler}
                />
            )
        }
    </tr>
    || <tr style={{display: "none"}}/>
;