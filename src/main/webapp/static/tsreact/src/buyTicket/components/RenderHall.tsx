import * as React from "react";
import {useEffect, useState} from "react";
import {TitleInfo} from "./TitleInfo";
import {getSessionId} from "../utils/utils";
import {HallLine} from "./HallLine";
import {IResponse, ResponseStatus} from "../interfaces/interfaces";
import {Navbar} from "../../components/Navbar";

export const RenderHall: React.FC = () => {
    const [hallLines, setHallLines] = useState([]);

    useEffect(loadHallData, []);

    function loadHallData() {
        const data = {sessionId: getSessionId()};
        //@ts-ignore
        $.post("/cinema/getHallLineList", data).done((response) => setHallLines(response.data));
    }

    function seatButtonClickHandler(seatId: number): void {

        let data = {
            sessionId: getSessionId(),
            hallRecordId: seatId
        };

        const responseHandler = (response: IResponse) => {
            if (response.status == ResponseStatus.OK) {
                loadHallData();
                return;
            }
            alert("Error:" + response.errorMessage);
        };
//@ts-ignore
        $.post("/cinema/checkReservation", data).done(responseHandler);
    }

    return (
        <div className="container-fluid">
            <Navbar/>
            <div className="row">
                <div className="col-md-4">
                    <TitleInfo/>
                </div>
                <div className="col-md-4">
                    <table className="table table-hover">
                        <tbody>
                        {
                            hallLines && hallLines.map((line) =>
                                <HallLine
                                    key={line.line}
                                    line={line.line}
                                    seatInfoList={line.seatInfoList}
                                    clickHandler={seatButtonClickHandler}
                                />
                            ) || <tr style={{display: "none"}}/>
                        }
                        </tbody>
                    </table>
                </div>
                <div className="col-md-8">
                </div>
            </div>
        </div>
    );
};


