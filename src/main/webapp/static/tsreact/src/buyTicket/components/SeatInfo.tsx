import * as React from "react";
import {ISeatInfo} from "../interfaces/interfaces";

const colors: {[key: string]: string} = {
    "0": "white",
    "1": "gray"
};

export const SeatInfo: React.FC<ISeatInfo> = (seatInfo) => {
    const color = colors["" + seatInfo.statusId] || 'white';

    return (
        <td>
            <button style={{backgroundColor: color}} onClick={() => seatInfo.clickHandler(seatInfo.seatId)}>
                {seatInfo.seat}
            </button>
        </td>
    )
}