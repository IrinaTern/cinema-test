import * as React from "react";
import {ITitleInfo} from "../interfaces/interfaces";
import moment = require("moment");
import {useEffect, useState} from "react";
import {getSessionId} from "../utils/utils";

export const TitleInfo: React.FC = () => {

    const [titleData, setTitleData] = useState<ITitleInfo>({dateAndTime: null, movieName: null, hallId: null});

    useEffect(loadTitleData, []);

    function loadTitleData() {
        const data = {sessionId: getSessionId()};
        //@ts-ignore
        $.post("/cinema/getTicketTitleBySessionId", data).done((response) => setTitleData({
                dateAndTime: response.data.sessionDateTime,
                movieName: response.data.movieName,
                hallId: response.data.hallId,
            })
        );
    }
    return (
        titleData.dateAndTime && titleData.movieName && titleData.hallId &&
        <table>
            <tbody>
            <tr>
                <td>Date and time:</td>
                <td>{moment(titleData.dateAndTime).format('L') + ' ' + moment(titleData.dateAndTime).format('LT')}</td>
            </tr>
            <tr>
                <td>Movie:</td>
                <td>{titleData.movieName}</td>
            </tr>
            <tr>
                <td>Hall:</td>
                <td>{titleData.hallId}</td>
            </tr>

            </tbody>
        </table>
        || <div style={{display: "none"}}/>
    )
}