export interface ISeatInfo extends IClickHandler<number>{
    seatId: number,
    seat: number,
    statusId: number
}

export interface ITitleInfo {
    dateAndTime: Date,
    movieName: string,
    hallId: number
}

export interface IHallLine extends IClickHandler<number>{
    line: number,
    seatInfoList: Array<ISeatInfo>
}

export interface IClickHandler<T> {
    clickHandler(id: T): void;
}

export interface IResponse {
    status: ResponseStatus,
    data: object,
    errorMessage: string
}

export enum ResponseStatus {
    ERROR = "ERROR",
    OK = "OK"
}
