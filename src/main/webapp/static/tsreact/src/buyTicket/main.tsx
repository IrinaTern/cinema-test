import * as React from "react";
import * as ReactDOM from "react-dom";

import {RenderHall} from "./components/RenderHall";


ReactDOM.render(
    <RenderHall/>,
    document.getElementById("my_test")
);