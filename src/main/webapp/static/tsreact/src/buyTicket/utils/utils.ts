

export function getSessionId(): string {
    const URL = document.location.href;
    const arr = URL.split('/');
    return arr[arr.length - 1];
}
