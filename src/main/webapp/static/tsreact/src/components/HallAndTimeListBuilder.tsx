import * as React from "react";
import {HallAndTimeList} from "../scheduleManagerForDate/components/ScheduleManagerForDate";
import {TimeListBuilder} from "./TimeListBuilder";

export const HallAndTimeListBuilder: React.FC<{ hallScheduleInfo: Array<HallAndTimeList> }> = (props) =>
    <>
        {
            props.hallScheduleInfo && props.hallScheduleInfo.map((hallAndTimeList) =>
                <>
                    <tr>
                        <td>
                            Hall {hallAndTimeList.hallId}
                        </td>
                        <TimeListBuilder timeList={hallAndTimeList.sessionIdTimeInfoList}/>
                    </tr>

                </>
            ) || <></>
        }
    </>
;