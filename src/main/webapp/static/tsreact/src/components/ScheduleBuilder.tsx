import * as React from "react";
import {MovieSchedule} from "../scheduleManagerForDate/components/ScheduleManagerForDate";
import {HallAndTimeListBuilder} from "./HallAndTimeListBuilder";

 export const ScheduleBuilder: React.FC<MovieSchedule> = (movieSchedule: MovieSchedule) =>
    <>
        <tr>
            <td>{movieSchedule.movieName}</td>
        </tr>
        <HallAndTimeListBuilder hallScheduleInfo={movieSchedule.hallScheduleInfo}/>
    </>
;