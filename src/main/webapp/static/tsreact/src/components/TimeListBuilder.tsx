import * as React from "react";
import {SessionIdAndStartTime} from "../scheduleManagerForDate/components/ScheduleManagerForDate";
import moment = require("moment");

export const TimeListBuilder: React.FC<{ timeList: Array<SessionIdAndStartTime>; }> = ({timeList}) =>
    timeList &&
    <>
        {
            timeList.map(sessionIdAndStartTime =>
                <td key={sessionIdAndStartTime.sessionId}>
                    <a href={"/cinema/buyTicket-react/" + sessionIdAndStartTime.sessionId}>
                        {moment(sessionIdAndStartTime.startTime).format("HH:mm")}
                    </a>
                </td>
            )
        }
    </>
    || <></>
;