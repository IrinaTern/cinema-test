import * as React from "react";
import * as ReactDOM from "react-dom";
import {ManePageManager} from "./components/ManePageManager";

ReactDOM.render(
  <ManePageManager/>,
    document.getElementById("main_page")
);