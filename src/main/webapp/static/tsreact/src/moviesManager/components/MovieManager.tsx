import * as React from "react";
import {ChangeEvent, useEffect, useState} from "react";
import {Navbar} from "../../components/Navbar";

export interface NewMovie {
    movieName: string;
    movieLength: string;
    movieDescription: string;
}

export interface Movie extends NewMovie {
    movieId: number;
}

export const MovieManager: React.FC = () => {
    console.log("MovieManager");
    const [movies, setMovies] = useState<Array<Movie>>([]);
    const [name, setName] = useState("");
    const [length, setLength] = useState("");
    const [description, setDescription] = useState("");

    useEffect(loadMovies, []);

    function saveNewMovie(movie: NewMovie) {
        //@ts-ignore
        $.post("/cinema/addNewMovie", movie).done(loadMovies);
    }

    function loadMovies() {
        //@ts-ignore
        $.post("/cinema/getAllMovie").done(setMovies);

    }


    function saveButtonClickHandler() {
        saveNewMovie({movieName: name, movieLength: length, movieDescription: description});
        setName("");
        setLength("");
        setDescription("");
    }

    function deleteBtnClickHandler(movie: Movie) {
        //@ts-ignore
        $.post("/cinema/deleteMovie", movie).done(loadMovies);

    }

    const buildHandler = (setter: (val: string) => void) => (e: ChangeEvent<HTMLInputElement>): void => {
        setter(e.target.value);
    };

    return (
        <div className="container-fluid">
            <Navbar/>
            <div className="row">
                <div className="col-md-12">
                    <label>
                        Movie name
                        <input
                            className="form-control"
                            type="text"
                            value={name}
                            onChange={(e: ChangeEvent<HTMLInputElement>) => {
                                setName(e.target.value);
                            }}
                        />
                    </label>
                    <label>
                        Movie length
                        <input
                            className="form-control"
                            type="number"
                            value={length}
                            onChange={buildHandler(setLength)}
                        />
                    </label>
                    <label>
                        Movie description
                        <input
                            className="form-control"
                            type="text"
                            value={description}
                            onChange={buildHandler(setDescription)}
                        />
                    </label>
                </div>
                <div className="col-md-12">
                    <button
                        type="button"
                        className="btn btn-primary"
                        onClick={saveButtonClickHandler}
                    >
                        Add new movie
                    </button>
                </div>
            </div>
            <hr/>
            <div className="row">
                <div className="table-scroll">
                    <div className="col-md-8">
                        <table className="table table-hover">
                            <thead>
                            <th scope="col">Name</th>
                            <th scope="col">Length</th>
                            <th scope="col">Description</th>
                            <th scope="col"></th>
                            </thead>
                            <tbody>
                            {
                                movies.map((movie) =>
                                    <tr key={movie.movieId}>
                                        <td>{movie.movieName}</td>
                                        <td>{movie.movieLength}</td>
                                        <td>{movie.movieDescription}</td>
                                        <td>
                                            <button
                                                onClick={() => deleteBtnClickHandler(movie)}
                                            >
                                                delete
                                            </button>
                                        </td>
                                    </tr>
                                )
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
};


