import * as React from "react";
import {ChangeEvent, useEffect, useState} from "react";
import {ScheduleBuilder} from "../../components/ScheduleBuilder";
import {Navbar} from "../../components/Navbar";

export interface SessionIdAndStartTime {
    sessionId: number;
    startTime: Date;
}

export interface HallAndTimeList {
    hallId: number;
    sessionIdTimeInfoList: Array<SessionIdAndStartTime>;
}

export interface MovieSchedule {
    movieId: number;
    movieName: string;
    movieLength: number;
    hallScheduleInfo: Array<HallAndTimeList>;
}

function dateToStr(date: Date): string {

    return `${date.getFullYear()}-${(date.getMonth() < 10 && "0" || "") + date.getMonth() + 1}-${date.getDate()}`
}

export const ScheduleManagerForDate: React.FC = () => {

    const [date, setDate] = useState(dateToStr(new Date()));
    const [schedule, setSchedule] = useState<Array<MovieSchedule>>([]);

    useEffect(getScheduleForDate, []);

    function getScheduleForDate() {
        //@ts-ignore
        $.post("/cinema/getScheduleForDate", {date}).done(setSchedule);
    }

    return (
        <div className="container-fluid">
            <Navbar/>
            <div className="row">
                <div className="col-md-4">

                    <label>Session's date
                        <input name="sessionDate"
                               className="form-control"
                               type="date"
                               value={date}
                               onChange={(e: ChangeEvent<HTMLInputElement>) => {
                                   setDate(e.target.value);
                               }}
                        />
                    </label>
                    <button className="btn  btn-success" onClick={getScheduleForDate}>
                        Show schedule
                    </button>

                    <table className="table table-hover">
                        <thead>
                        <th scope="col">
                            Sessions for selected date
                        </th>
                        </thead>
                        <tbody>

                        {
                            schedule && schedule.map(sc =>
                                <ScheduleBuilder key={sc.movieId} {...sc}/>
                            ) || <span>Loading...</span>
                        }
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    )
};
