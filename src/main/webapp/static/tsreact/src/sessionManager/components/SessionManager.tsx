import * as React from "react";
import {ChangeEvent, useEffect, useState} from "react";
import {Movie} from "../../moviesManager/components/MovieManager";
import {IResponse} from "../../buyTicket/interfaces/interfaces";
import {MovieSchedule} from "../../scheduleManagerForDate/components/ScheduleManagerForDate";
import {ScheduleBuilder} from "../../components/ScheduleBuilder";
import {Navbar} from "../../components/Navbar";

interface NewSession {
    movieId: number,
    hallId: number,
    sessionDateTime: number,
    price: number
}

export const SessionManager: React.FC = () => {

    const [movies, setMovies] = useState<Array<Movie>>([]);
    const [halls, setHalls] = useState<Array<number>>([]);

    const [movieId, setMovieId] = useState<number>(null);
    const [hallId, setHallId] = useState<number>(null);
    const [sessionDateTime, setSessionDateTime] = useState<string>("");
    const [price, setPrice] = useState<number>(null);

    const [schedule, setSchedule] = useState<Array<MovieSchedule>>([]);

    useEffect(loadMoviesNames, []);
    useEffect(loadHalls, []);

    function loadMoviesNames() {
        //@ts-ignore
        $.post("/cinema/getAllMovie").done(setMovies);
    }

    function loadHalls() {
        //@ts-ignore
        $.post("/cinema/getHallList").done(setHalls);
    }

    function saveButtonClickHandler() {
        saveNewSession({movieId: movieId, hallId: hallId, sessionDateTime: Date.parse(sessionDateTime), price: price});
    }

    function saveNewSession(session: NewSession) {
        //@ts-ignore
        $.post("/cinema/addSession", session).done(responseHandler);

        function responseHandler(response: IResponse) {
            if (response.status === "OK") {
                alert("Session added");
                refreshSessionsTable();
                return;
            }
            alert("Error:" + response.errorMessage);
        }
    }

    function refreshSessionsTable() {
        loadSessionsForSelectedDate();
    }

    function loadSessionsForSelectedDate() {
        let date: Date = new Date(Date.parse(sessionDateTime));

        let data = {
            date: dateToStr(date)
        }
        //@ts-ignore
        $.post("/cinema/getScheduleForDate", data).done(setSchedule);
    }

    function dateToStr(date: Date): string {
        return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
    }
    return (
        <div className="container-fluid">
            <Navbar/>
            <div className="row add-film-form">
                <div className="col-md-4 form">
                    <div className="row">
                        <div className="col">
                            <label>Session's date
                                <input id="datetime"
                                       className="form-control"
                                       type="datetime-local"
                                       value={sessionDateTime}
                                       onChange={(e: ChangeEvent<HTMLInputElement>) => {
                                           console.log(e.target.value);
                                           setSessionDateTime(e.target.value);
                                       }}
                                />
                            </label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 form">
                            <select name="movieName"
                                    className="form-control form-session-el"
                                    value={movieId}
                                    onChange={(e: ChangeEvent<HTMLSelectElement>) => {
                                        setMovieId(parseInt(e.target.value));
                                    }}
                            >
                                {
                                    movies.map((movie) =>
                                        <option value={movie.movieId}>
                                            {movie.movieName}
                                        </option>)
                                }
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 form">
                            <select name="hallId"
                                    className="form-control form-session-el"
                                    value={hallId}
                                    onChange={(e: ChangeEvent<HTMLSelectElement>) => {
                                        setHallId(parseInt(e.target.value));
                                    }}
                            >
                                {
                                    halls.map((hall) =>
                                        <option
                                            value={hall}>
                                            {hall}
                                        </option>)
                                }
                            </select>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-6 form">
                            <label>Price
                                <input
                                    className="form-control"
                                    type="text"
                                    value={price}
                                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                                        setPrice(parseInt(e.target.value));
                                    }}
                                />
                            </label>
                        </div>
                    </div>

                    <div className="row">

                    </div>
                    <div className="row">
                        <div className="col">
                            <button className="btn  btn-success form-session-el"
                                    id="add-session-btn"
                                    onClick={saveButtonClickHandler}>
                                Add Session
                            </button>
                        </div>
                    </div>
                </div>
                <div className="col-md-6 table-scroll">
                    <div className="col film-table">
                        <table className="table table-hover">
                            <thead>
                            <th scope="col">Sessions for selected date</th>
                            </thead>
                            <tbody>
                            {
                                schedule && schedule.map(sc =>
                                    <ScheduleBuilder key={sc.movieId} {...sc}/>
                                ) || <span>Loading...</span>
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
}