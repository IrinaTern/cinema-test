import * as React from "react";
import * as ReactDOM from "react-dom";
import {SessionManager} from "./components/SessionManager";

ReactDOM.render(
    <SessionManager/>,
    document.getElementById("session_manager")
);