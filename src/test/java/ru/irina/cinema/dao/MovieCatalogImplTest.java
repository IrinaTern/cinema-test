package ru.irina.cinema.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ru.irina.cinema.jdo.Movie;
import ru.irina.cinema.utils.FileUtils;

import java.util.List;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class MovieCatalogImplTest {

    private final String initSql = FileUtils.loadScriptFromFile("/sql/init.sql");

    @Before
    public void init() {
        operations.execute(initSql);
    }

    @Autowired
    private JdbcOperations operations;

    @Autowired
    MovieCatalogDao movieCatalog;

    @Test
    public void getAllMovies() {
        assert movieCatalog.getAllMovies().size()==0;

        operations.execute("insert into movie_catalog (MOVIE_ID, MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values (1,'Kill Bill', 120, 'Wow')");
        operations.execute("insert into movie_catalog(MOVIE_ID, MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values (2,'Kill Bill 2', 120, 'Wow')");

        List<Movie> testList = movieCatalog.getAllMovies();
        assert testList.size()==2;
    }

    @Test
    public void getMovieById() {
        operations.execute("insert into movie_catalog (MOVIE_ID, MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values (1,'Kill Bill', 120, 'Wow')");
        operations.execute("insert into movie_catalog(MOVIE_ID, MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values (2,'Kill Bill 2', 120, 'Wow')");

        Movie movie = movieCatalog.getMovieById(1L);
        assert movie.getMovieName().equals("Kill Bill");
        assert movie.getMovieId()==1;

    }

    @Test
    public void addMovie() {
        assert movieCatalog.getAllMovies().size()==0;
        movieCatalog.addMovie("Kill Bill", 120, "Wow");
        assert movieCatalog.getAllMovies().size()==1;
    }




    @Test
    public void deleteMovie() {
        assert movieCatalog.getAllMovies().size()==0;

        operations.execute("insert into movie_catalog (MOVIE_ID, MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values (1,'Kill Bill', 120, 'Wow')");
        operations.execute("insert into movie_catalog (MOVIE_ID, MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values (2,'Kill Bill 2', 120, 'Wow')");
        movieCatalog.deleteMovie(1L);
        assert movieCatalog.getAllMovies().size()==1;
    }
}