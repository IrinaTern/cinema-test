package ru.irina.cinema.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ru.irina.cinema.jdo.Schedule;
import ru.irina.cinema.utils.FileUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class ScheduleImplTest {
    private final String initSql = FileUtils.loadScriptFromFile("/sql/init.sql");
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    @Before
    public void init() {
        operations.execute(initSql);
    }
    @Autowired
    private JdbcOperations operations;

    @Autowired
    ScheduleDao scheduleDao;


    @Test
    public void getScheduleList() throws ParseException {
        assert scheduleDao.getScheduleList(formatter.parse("2019-06-30"), formatter.parse("2019-07-01")).size()==0;
        operations.execute("insert into movie_catalog (MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values ('a', 120, 'Wow') ");
        operations.execute("insert into movie_catalog (MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values ('b', 120, 'Wow') ");
        operations.execute("insert into halls (HALL_RECORD_ID, HALL_ID, LINE, SEAT) values (5, 1, 2, 5) ");
        operations.execute("insert into sessions (SESSION_ID, SESSION_DATE_TIME, MOVIE_ID, HALL_ID) values (sequence_session_id.nextval, '2019-06-30',1,1) ");
        operations.execute("insert into sessions (SESSION_ID, SESSION_DATE_TIME, MOVIE_ID, HALL_ID) values (sequence_session_id.nextval, '2019-07-01',2,1) ");
        operations.execute("insert into sessions (SESSION_ID, SESSION_DATE_TIME, MOVIE_ID, HALL_ID) values (sequence_session_id.nextval, '2019-06-28',2,1) ");
        operations.execute("insert into sessions (SESSION_ID, SESSION_DATE_TIME, MOVIE_ID, HALL_ID) values (sequence_session_id.nextval, '2019-06-29',2,2) ");

        assert scheduleDao.getScheduleList(formatter.parse("2019-06-30"), formatter.parse("2019-07-01")).size()==2;
        assert scheduleDao.getScheduleList(formatter.parse("2019-06-30"), formatter.parse("2019-07-01")).get(0).getMovieName().equals("a");



    }
}