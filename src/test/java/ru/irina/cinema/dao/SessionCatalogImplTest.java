package ru.irina.cinema.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ru.irina.cinema.utils.FileUtils;

import java.util.Date;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class SessionCatalogImplTest {
    private final String initSql = FileUtils.loadScriptFromFile("/sql/init.sql");

    @Before
    public void init() {
        operations.execute(initSql);
    }
    @Autowired
    private JdbcOperations operations;

    @Autowired
    SessionCatalogDao sessionCatalog;

    @Test
    public void addMovieToSchedule() {
        assert sessionCatalog.getAllSchedule().size()==0;
        operations.execute("insert into movie_catalog (MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values ('Kill Bill', 120, 'Wow') ");
        operations.execute("insert into movie_catalog (MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values ('Kill Bill 2', 120, 'Wow') ");
        operations.execute("insert into halls (HALL_RECORD_ID, HALL_ID, LINE, SEAT) values (5, 1, 2, 5) ");
        sessionCatalog.addMovieToSchedule(new Date(), 1L, 1L);
        assert sessionCatalog.getAllSchedule().size()==1;

    }

    @Test
    public void deleteMovieFromSchedule() {
        assert sessionCatalog.getAllSchedule().size()==0;
        operations.execute("insert into movie_catalog (MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values ('Kill Bill', 120, 'Wow') ");
        operations.execute("insert into movie_catalog (MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values ('Kill Bill 2', 120, 'Wow') ");
        operations.execute("insert into halls (HALL_RECORD_ID, HALL_ID, LINE, SEAT) values (5, 1, 2, 5) ");
        sessionCatalog.addMovieToSchedule(new Date(), 1L, 1L);
        assert sessionCatalog.getAllSchedule().size()==1;
        sessionCatalog.deleteMovieFromSchedule(1L);
        assert sessionCatalog.getAllSchedule().size()==0;

    }

    @Test
    public void getAllSchedule() {
        assert sessionCatalog.getAllSchedule().size()==0;
        operations.execute("insert into movie_catalog (MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values ('Kill Bill', 120, 'Wow') ");
        operations.execute("insert into movie_catalog (MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values ('Kill Bill 2', 120, 'Wow') ");
        operations.execute("insert into halls (HALL_RECORD_ID, HALL_ID, LINE, SEAT) values (5, 1, 2, 5) ");

        sessionCatalog.addMovieToSchedule(new Date(), 1L, 1L);

    }
}