package ru.irina.cinema.dao;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ru.irina.cinema.utils.FileUtils;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class TicketDaoImplTest {

    private final String initSql = FileUtils.loadScriptFromFile("/sql/init.sql");

    @Autowired
    private JdbcOperations operations;

    @Autowired
    private TicketDao ticketDao;

    @Before
    public void init(){
        operations.execute(initSql);
    }

    @Test
    public void getSeatStatusId() {

        operations.execute("insert into movie_catalog (MOVIE_ID, MOVIE_NAME, MOVIE_LENGTH, MOVIE_DESCRIPTION) values (1,'Kill Bill', 120, 'Wow')");
        operations.execute("insert into halls (hall_record_id, hall_id, line, seat) values (1, 1, 2, 5)");
        operations.execute("insert into halls (hall_record_id, hall_id, line, seat) values (2, 1, 4, 6)");
        operations.execute("insert into halls (hall_record_id, hall_id, line, seat) values (3, 1, 7, 8)");
        operations.execute("insert into sessions (session_id, session_date_time, movie_id, hall_id, price) values (sequence_session_id.nextval,'2019-06-30',1,1, 400)");
        operations.execute("insert into tickets (session_id, status_id, hall_record_id, price) values (sequence_session_id.currval,2,1,300)");
        operations.execute("insert into tickets (session_id, status_id, hall_record_id, price) values (sequence_session_id.currval,0,2,200)");
        operations.execute("insert into tickets (session_id, status_id, hall_record_id, price) values (sequence_session_id.currval,1,3,200)");

        assert ticketDao.getSeatStatusId(1L, 2L) == 0;
        assert ticketDao.getSeatStatusId(1L, 3L) == 1;

    }
}