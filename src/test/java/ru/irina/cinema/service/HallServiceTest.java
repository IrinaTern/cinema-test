package ru.irina.cinema.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ru.irina.cinema.dao.HallDao;
import ru.irina.cinema.dao.ScheduleDao;
import ru.irina.cinema.exceptions.InvalidHallDataException;
import ru.irina.cinema.jdo.HallLine;
import ru.irina.cinema.jdo.HallRawData;
import ru.irina.cinema.jdo.SeatInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class HallServiceTest {

    @Mock
    private HallDao hallDao;

    @InjectMocks
    private HallService hallService;


    @Test(expected = InvalidHallDataException.class)
    public void checkRawData_nullData() {
        HallService.checkRawData(null);
    }

    @Test(expected = InvalidHallDataException.class)
    public void checkRawData_emptyList() {
        HallService.checkRawData(Collections.emptyList());
    }

    @Test(expected = InvalidHallDataException.class)
    public void checkRawData_nullHallRecordId() {
        HallService.checkRawData(Arrays.asList(
                new HallRawData(1L, 1L, 1, 1,0),
                new HallRawData(null, 1L, 1, 1, 0)
                )
        );

    }

    @Test(expected = InvalidHallDataException.class)
    public void checkRawData_nullHallId() {
        HallService.checkRawData(Arrays.asList(
                new HallRawData(1L, 1L, 1, 1, 1),
                new HallRawData(1L, null, 1, 1, 1)
                )
        );

    }

    @Test(expected = InvalidHallDataException.class)
    public void checkRawData_nullLine() {
        HallService.checkRawData(Arrays.asList(
                new HallRawData(1L, 1L, 1, 1, 0),
                new HallRawData(1L, 1L, null, 1, 1)
                )
        );

    }

    @Test(expected = InvalidHallDataException.class)
    public void checkRawData_nullSeat() {
        HallService.checkRawData(Arrays.asList(
                new HallRawData(1L, 1L, 1, 1, 0),
                new HallRawData(1L, 1L, 1, null, 0)
                )
        );

    }

    @Test
    public void checkRawData_valid() {
        HallService.checkRawData(Arrays.asList(
                new HallRawData(1L, 1L, 1, 1, 0),
                new HallRawData(2L, 1L, 1, 1, 0)
                )
        );

    }


    @Test
    public void getHallLine() {
        List<HallRawData> testData = Arrays.asList(
                new HallRawData(1L, 1L, 1, 7, 0),
                new HallRawData(2L, 1L, 1, 8, 0),
                new HallRawData(3L, 1L, 1, 9, 0),
                new HallRawData(4L, 1L, 2, 10, 0),
                new HallRawData(5L, 1L, 3, 11, 0),
                new HallRawData(6L, 1L, 4, 1, 1)
        );
        HallLine hallLine = new HallLine();
        hallLine.setLine(1);
        hallLine.setSeatInfoList(
                Arrays.asList(
                        new SeatInfo(1L, 7, 0),
                        new SeatInfo(2L, 8, 0),
                        new SeatInfo(3L, 9, 0)
                )
        );

        assert HallService.getHallLine(testData, 1).equals(hallLine);


    }

    @Test
    public void getHallLineList() {

        List <HallRawData> hallRawDataList = Arrays.asList(
                new HallRawData(1L, 1L, 1, 1, 0),
                new HallRawData(2L, 1L, 1, 2, 0),
                new HallRawData(3L, 1L, 1, 3, 0),
                new HallRawData(4L, 1L, 2, 1, 0),
                new HallRawData(5L, 1L, 2, 2, 1),
                new HallRawData(6L, 1L, 2, 3, 1)
        );

        when(hallDao.getAllSeatsBySessionId(any())).thenReturn(hallRawDataList);
        List<HallLine> result = hallService.getHallLineList(1L);

        List<HallLine> testData = Arrays.asList(
                new HallLine(1, Arrays.asList(
                        new SeatInfo(1L, 1, 0),
                        new SeatInfo(2L, 2, 0),
                        new SeatInfo(3L, 3, 0)
                )),
                new HallLine(2, Arrays.asList(
                        new SeatInfo(4L, 1, 0),
                        new SeatInfo(5L, 2, 1),
                        new SeatInfo(6L, 3, 1)
                ))
        );

        assertEquals(result, testData);

    }
}