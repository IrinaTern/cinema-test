package ru.irina.cinema.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import ru.irina.cinema.dao.ScheduleDao;
import ru.irina.cinema.jdo.FilmSheduleInfo;
import ru.irina.cinema.jdo.Schedule;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest
public class ScheduleServiceTest {


    @Mock
    private ScheduleDao scheduleDao;

    @InjectMocks
    private ScheduleService scheduleService;

    @Before
    public void setUp()  {

    }

    @Test
    public void getScheduleToDates()  {
        List<Schedule> scheduleList = Arrays.asList(
                new Schedule(1L, "a", 120, 1L, null, 1L),
                new Schedule(1L, "a", 120, 1L, null, 2L),
                new Schedule(1L, "a", 120, 1L, null, 3L),
                new Schedule(1L, "a", 120, 2L, null, 4L),
                new Schedule(1L, "a", 120, 2L, null, 5L),
                new Schedule(2L, "b", 120, 2L, null, 6L),
                new Schedule(2L, "b", 120, 3L, null, 7L),
                new Schedule(2L, "b", 120, 3L, null, 8L),
                new Schedule(2L, "b", 120, 3L, null, 9L)
        );

        when(scheduleDao.getScheduleList(any(), any())).thenReturn(scheduleList);

        List<FilmSheduleInfo> result = scheduleService.getScheduleToDates(new Date(), new Date());

        assert result.size() == 2;
        assert result.get(0).getMovieId() == 1;
        assert result.get(0).getHallScheduleInfo().size() == 2;
        assert result.get(0).getHallScheduleInfo().get(0).getHallId() == 1;
        assert result.get(0).getHallScheduleInfo().get(0).getSessionIdTimeInfoList().size() == 3;
        assert result.get(0).getHallScheduleInfo().get(1).getHallId() == 2;
        assert result.get(0).getHallScheduleInfo().get(1).getSessionIdTimeInfoList().size() == 2;

        assert result.get(1).getMovieId() == 2;
        assert result.get(1).getHallScheduleInfo().size() == 2;
        assert result.get(1).getHallScheduleInfo().get(0).getHallId() == 2;
        assert result.get(1).getHallScheduleInfo().get(0).getSessionIdTimeInfoList().size() == 1;
        assert result.get(1).getHallScheduleInfo().get(1).getHallId() == 3;
        assert result.get(1).getHallScheduleInfo().get(1).getSessionIdTimeInfoList().size() == 3;


    }


}

